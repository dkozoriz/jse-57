package ru.t1.dkozoriz.tm.repository.model.business;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.dkozoriz.tm.api.repository.model.ITaskRepository;
import ru.t1.dkozoriz.tm.model.business.Task;

import java.util.List;

@Repository
@Scope("prototype")
@NoArgsConstructor
public final class TaskRepository extends BusinessRepository<Task> implements ITaskRepository {

    @Override
    @NotNull
    protected Class<Task> getClazz() {
        return Task.class;
    }

    @Override
    @NotNull
    public List<Task> findAllByProjectId(@Nullable final String userId, @NotNull final String projectId) {
        @NotNull final String jpql = "SELECT m FROM Task m WHERE m.userId = :userId AND m.projectId = :projectId";
        return entityManager.createQuery(jpql, Task.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

}