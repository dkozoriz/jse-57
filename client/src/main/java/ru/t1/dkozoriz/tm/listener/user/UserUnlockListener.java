package ru.t1.dkozoriz.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkozoriz.tm.dto.request.user.UserUnlockRequest;
import ru.t1.dkozoriz.tm.event.ConsoleEvent;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

@Component
public final class UserUnlockListener extends AbstractUserListener {

    public UserUnlockListener() {
        super("user-unlock", "user unlock.");
    }

    @Override
    @EventListener(condition = "@userUnlockListener.getName() == #consoleEvent.name")
    public void handler(@NotNull ConsoleEvent consoleEvent) {
        System.out.println("[USER UNLOCK]");
        System.out.println("ENTER LOGIN:");
        @Nullable final String login = TerminalUtil.nextLine();
        userEndpoint.userUnlock(new UserUnlockRequest(getToken(), login));
    }
}
