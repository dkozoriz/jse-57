package ru.t1.dkozoriz.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.dkozoriz.tm.api.repository.dto.IUserOwnedDtoRepository;
import ru.t1.dkozoriz.tm.api.repository.model.IUserOwnedRepository;
import ru.t1.dkozoriz.tm.api.service.IServiceLocator;
import ru.t1.dkozoriz.tm.api.service.model.IUserOwnedService;
import ru.t1.dkozoriz.tm.exception.field.IdEmptyException;
import ru.t1.dkozoriz.tm.exception.field.IndexIncorrectException;
import ru.t1.dkozoriz.tm.model.UserOwnedModel;

import javax.persistence.EntityManager;
import java.util.List;

@Service
public abstract class UserOwnedService<T extends UserOwnedModel> extends AbstractService<T> implements IUserOwnedService<T> {

    @NotNull
    protected abstract IUserOwnedRepository<T> getRepository();

    @Override
    @Nullable
    public T add(@NotNull final String userId, @Nullable final T model) {
        if (model == null) return null;
        @NotNull final IUserOwnedRepository<T> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.add(userId, model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return model;
    }

    @Override
    public void update(@NotNull final String userId, @NotNull final T model) {
        try {
            @NotNull final IUserOwnedRepository<T> repository = getRepository();
            @NotNull final EntityManager entityManager = repository.getEntityManager();
            entityManager.getTransaction().begin();
            repository.update(userId, model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final IUserOwnedRepository<T> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.clear(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @NotNull
    public List<T> findAll(@NotNull final String userId) {
        @NotNull final IUserOwnedRepository<T> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findAll(userId);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void remove(@NotNull final String userId, @Nullable final T model) {
        @NotNull final IUserOwnedRepository<T> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.remove(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final T model = findById(userId, id);
        if (model == null) return;
        remove(userId, model);
    }

    @Override
    public void removeByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index <= 0 || index > getSize(userId)) throw new IndexIncorrectException();
        @Nullable final T model = findByIndex(userId, index);
        if (model == null) return;
        remove(userId, model);
    }

    @Override
    @Nullable
    public T findById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        @NotNull final IUserOwnedRepository<T> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findById(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @Nullable
    public T findByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index <= 0 || index > getSize(userId)) throw new IndexIncorrectException();
        @NotNull final IUserOwnedRepository<T> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findByIndex(userId, index);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public int getSize(@NotNull final String userId) {
        @NotNull final IUserOwnedRepository<T> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.getSize(userId);
        } finally {
            entityManager.close();
        }
    }

}