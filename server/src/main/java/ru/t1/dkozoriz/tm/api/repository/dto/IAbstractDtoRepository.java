package ru.t1.dkozoriz.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.dto.model.AbstractModelDto;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

public interface
IAbstractDtoRepository<T extends AbstractModelDto> {

    @NotNull
    EntityManager getEntityManager();

    void add(T model);

    void update(T model);

    void clear();

    @Nullable
    List<T> findAll();

    @Nullable
    T findById(@NotNull String id);

    @Nullable
    T findByIndex(@NotNull Integer index);

    long getSize();

    void remove(T model);

    void removeAll(@NotNull Collection<T> models);

    void addAll(@NotNull Collection<T> models);

    void set(@NotNull Collection<T> models);

}