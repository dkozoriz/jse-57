package ru.t1.dkozoriz.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkozoriz.tm.dto.model.business.TaskDto;
import ru.t1.dkozoriz.tm.dto.request.task.TaskShowByIndexRequest;
import ru.t1.dkozoriz.tm.event.ConsoleEvent;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

@Component
public final class TaskShowByIndexListener extends AbstractTaskListener {

    public TaskShowByIndexListener() {
        super("task-show-by-index", "show task by index.");
    }

    @Override
    @EventListener(condition = "@taskShowByIndexListener.getName() == #consoleEvent.name")
    public void handler(@NotNull ConsoleEvent consoleEvent) {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        @Nullable final TaskDto task =
                taskEndpoint.taskShowByIndex(new TaskShowByIndexRequest(getToken(), index)).getTask();
        showTask(task);
    }

}