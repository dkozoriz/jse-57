package ru.t1.dkozoriz.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkozoriz.tm.dto.request.task.TaskStartByIndexRequest;
import ru.t1.dkozoriz.tm.event.ConsoleEvent;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

@Component
public final class TaskStartByIndexListener extends AbstractTaskListener {

    public TaskStartByIndexListener() {
        super("task-start-by-index", "start task by index.");
    }

    @Override
    @EventListener(condition = "@taskStartByIndexListener.getName() == #consoleEvent.name")
    public void handler(@NotNull ConsoleEvent consoleEvent) {
        System.out.println("[START TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        taskEndpoint.taskStartByIndex(new TaskStartByIndexRequest(getToken(), index));
    }

}